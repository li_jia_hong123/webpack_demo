import "bootstrap/dist/js/bootstrap.js";
import "animate.css";
import "./index.scss";

// API
import { getMenu } from "@/api/index";

// 加载公共头部
$("#head").load("../../common/html/head.html");

/* 逻辑代码 */
$("#send").on("click", async () => {
  let res = await getMenu();
  console.log(res);
});
