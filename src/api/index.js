import request from "@/utils/request"

export function getMenu(params){
    return request({
        url:'/article/list',
        method:'get',
        params
    })
}