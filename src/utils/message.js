/***
 * @param {String} msg 信息提示
 * @param {String} type 信息提示类型[info(默认),success,warning,danger]
 * @param {Number} msg 多久消失
 * @param {String|Object}  parentNode 要插入的父节点标签或者jQuery元素(默认body)
 * @param {boolean} msg 信息提示内容是否居中
 */
export function successAlert(
  msg,
  type = "info",
  wait = 1500,
  parentNode,
  center = true
) {
  let $alert = $(
    `<div class="alert alert-${type} alert-message ${center
      ? "center"
      : ""}" role="alert">${msg}</div>`
  );
  if (parentNode) {
    if (!(parentNode instanceof $)) {
      parentNode = $("parentNode");
    }
    parentNode.append($alert);
  } else {
    $("body").append($alert);
    setTimeout(() => {
      $alert.remove();
    }, wait);
  }
}
