const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path')

function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = merge(common, {
    mode: "production",
    plugins: [
        new MiniCssExtractPlugin({
            experimentalUseImportModule: true,
            filename: 'css/[name].[contenthash:8].css',
        }),
        new UglifyJsPlugin({
            uglifyOptions: {
                compress: {
                    drop_console: true
                }
            }
        })
    ],
    module: {
        rules: [{
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader']
            },
            {
                test: /\.scss$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader', 'postcss-loader']
            },
            {
                test: /\.less$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'less-loader', 'postcss-loader']
            },
            {
                test: /\.(jpe?g|png|gif|bmp|svg|webp|jfif)$/i,
                type: 'asset', //自动选择导出为单独文件或者 dataURL形式（默认为8KB）. 之前有url-loader设置asset size limit 限制实现
                parser: {
                    dataUrlCondition: {
                        maxSize: 8 * 1024 // 8kb  指定大小
                    }
                },
                generator: {
                    filename: 'image/[name].[contenthash:8][ext][query]'
                }
            },
            {
                test: /\.(ttf|eot|woff|woff2)$/i,
                type: 'asset/resource',
                generator: {
                    filename: 'font/[name].[contenthash:8][ext][query]'
                }
            },
        ],
    },
    optimization: {
        /* 模块拆分 */
        splitChunks: {
            chunks: 'all', // 针对所有模块
            minSize: 20000, // 要生成的块的最小大小（以字节为单位）。
            maxSize: 150000,
            minChunks: 2, // 在拆分之前，模块必须在chunk之间共享的最小次数
            cacheGroups: {
                libs: {
                    name: 'chunk-libs',
                    test: /[\\/]node_modules[\\/]/,
                    priority: 10,
                    chunks: 'initial' // only package third parties that are initially dependent
                },
                jquery: {
                    name: 'chunk-jquery', // split elementUI into a single package
                    minChunks: 1, // 在拆分之前，模块必须在chunk之间共享的最小次数
                    priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
                    test: /[\\/]node_modules[\\/]_?jquery(.*)/ // in order to adapt to cnpm
                },
                bootstrap: {
                    name: 'chunk-bootstrap', // split elementUI into a single package
                    minChunks: 1, // 在拆分之前，模块必须在chunk之间共享的最小次数
                    priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
                    test: /[\\/]node_modules[\\/]_?bootstrap(.*)/ // in order to adapt to cnpm
                },
                commons: {
                    chunks: 'initial', //initial表示提取入口文件的公共部分
                    minChunks: 2, //表示提取公共部分最少的文件数
                    minSize: 0, //表示提取公共部分最小的大小
                    name: 'commons' //提取出来的文件命名
                },
                dayjs: {
                    chunks: 'chunk-time', //initial表示提取入口文件的公共部分
                    minChunks: 1, //表示提取公共部分最少的文件数
                    priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
                    test: /[\\/]node_modules[\\/]_?dayjs(.*)/ // in order to adapt to cnpm
                },
                UA:{
                    chunks: 'chunk-ua', //initial表示提取入口文件的公共部分
                    minChunks: 1, //表示提取公共部分最少的文件数
                    priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
                    test: /[\\/]node_modules[\\/]_?ua-device(.*)/ // in order to adapt to cnpm
                }
            }
        },
    },
})