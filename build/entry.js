const path = require('path');
const fs = require('fs');
let baseDir = path.join(__dirname, "..", "src", "pages")
module.exports = () => {
    let dirnames = fs.readdirSync(baseDir)
    let obj = {};
    dirnames.map(dirname => {
        // obj[dirname] = `${baseDir}/${dirname}/${dirname}.js`
        obj[dirname] = path.join(baseDir, dirname, dirname + '.js')
    })
    return obj
}