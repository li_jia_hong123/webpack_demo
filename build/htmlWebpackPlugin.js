const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

let pagesPath = path.join(__dirname, '..', 'src', 'pages')
let folders = fs.readdirSync(pagesPath)
let plugin = folders.map(item => new HtmlWebpackPlugin({
    template: path.join(pagesPath, item, item + '.html'),
    filename: `${item}.html`, // 输出的html文件
    chunks: [item], // 引入自己的js文件,若不加这行,会全部引入
    favicon: path.join(__dirname, "../src/assets/favicon.ico"),
    meta: {
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
        content: 'width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,viewport-fit=cover'
    },
    hash: true,
    minify: {
        collapseWhitespace: true, // 折叠空行
        removeAttributeQuotes: true // 移除属性的引号
    }
}))
plugin.unshift(new CleanWebpackPlugin())
module.exports = plugin

