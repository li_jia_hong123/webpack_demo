const path = require('path');
const entry = require('./entry');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('./htmlWebpackPlugin');
// 尝试使用环境变量，否则使用根路径
const ASSET_PATH = process.env.ASSET_PATH || '/';



module.exports = {
    entry: entry(),
    output: {
        clean: true, // 在生成文件之前清空 output 目录
        path: path.resolve(__dirname, '../dist'),
        filename: 'js/[name].[contenthash:8].bundle.js',
        // assetModuleFilename: 'assets/[name].[contenthash:8][ext][query]', // loader里面单独写了
        publicPath: ASSET_PATH,
    },
    resolve: {
        modules: ['node_modules'], // 告诉 webpack 解析模块时应该搜索的目录。
        extensions: ['.js', '.json', '.html'],
        alias: {
            '@': path.resolve(__dirname, '..', 'src'),
            "~pages": path.resolve(__dirname, '..', 'src', 'pages')
        },
    },
    module: {
        rules: [{
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ["@babel/plugin-transform-runtime",'@babel/plugin-proposal-object-rest-spread']
                    }
                }
            },
            {
                test: /\.html$/i,
                loader: "html-loader",
            },
        ]
    },
    plugins: [
        ...HtmlWebpackPlugin,
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            'window.$': 'jquery',
            'window.jQuery': 'jquery'
        })
    ]
}