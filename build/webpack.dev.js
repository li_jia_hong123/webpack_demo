const {
    merge
} = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir)
}
module.exports = merge(common, {
    mode: 'development',
    devServer: {
        static: resolve('../src'), // 静态资源目录
        compress: true, // gzip开启
        hot: true,
        open: ['/index.html'],
        port: 'auto', // 端口 或者 auto
        allowedHosts: 'auto', // 允许 localhost、 host 和 client.webSocketURL.hostname
        client: {
            progress: true,
            overlay: true, // 当出现编译错误或警告时，在浏览器中显示全屏覆盖。
        },
        // // 请求代理,解决开发环境跨域
        // proxy: { 
        //     '/api': {
        //         target: 'http://localhost:3000',
        //         pathRewrite: { '^/api': '' },
        //     },
        // },
    },
    watchOptions: {
        aggregateTimeout: 600,
        ignored: /node_modules/,
    },
    devtool: "eval-source-map",
    module: {
        rules: [{
                test: /\.css$/,
                use: ['style-loader', 'css-loader', 'postcss-loader']
            },
            {
                test: /\.(scss|sass)$/,
                use: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader']
            },
            {
                test: /\.less$/,
                use: ['style-loader', 'css-loader', 'less-loader', 'postcss-loader']
            },
            {
                test: /\.(jpe?g|png|gif|bmp|svg|webp|jfif|ttf|eot|woff|woff2)$/i,
                type: 'asset/inline'
            },

        ]
    },
    stats: {
        children: true
    }
})