# webpack5 + jquery配置的一个多页面多入口工程

## 版本 
+ "webpack": "^5.54.0"
+ "jquery": "^1.12.4"

安装依赖
```bash
$ npm i
```
开发
```bash
$ npm run dev
```

打包
```bash
$ npm run build
```

## 新建页面
+ 步骤一. `src/pages`下新建页面文件夹`about`(必须)
+ 步骤二. 在 `about`文件夹子下新建页面`about.html`(必须)
+ 步骤三. 在 `about`文件夹子下新建页面入口文件`about.js`(必须)
+ 步骤四. 页面自己的js,样式文件,等文件可以放到该页面的文件夹(非必须)


## 功能

1. 集成了scss, less,可直接使用,在入口文件引入即可,例如:
```js
// pages/index/index.js
import "@/common/scss/common.scss";
```

2. 配置了路径解析
`@` 代表src目录
`~pages` 代表pages目录

3. 生产环境下移除了所有console

4. 集成了dayjs,用于处理时间日期,用法同moment
```javascript
// 页面入口文件
import dayjs from 'dayjs';
```
5. 加入了ua-device,用于网站数据上报

6. 添加自动添加浏览器兼容性前缀功能

7. 引入了bootstrap@3.4.1版本

8. 添加了请求发送等信息

##  目录结构
```
|-- 项目根目录                                                    
    |-- .gitignore                  
    |-- package-lock.json           
    |-- package.json                
    |-- postcss.config.js           
    |-- node_modules\                       
    |-- README.md
    |-- build\                      webpack配置目录
    |   |-- entry.js                打包入口文件
    |   |-- htmlWebpackPlugin.js    htmlWebpackPlugin插件生成器文件
    |   |-- webpack.common.js       webpack公共配置
    |   |-- webpack.dev.js          webpack开发配置
    |   |-- webpack.prod.js         webpack生成配置
    |-- dist\                       打包输出目录
    |-- src\                        源代码目录
        |-- assets\                 项目静态资源
        |   |-- favicon.ico         ico图标
        |   |-- font\               项目字体图标
        |   |-- img\                项目用到的静态图片
        |-- common                  项目公共文件
        |   |-- html\               公共html目录
        |   |-- js\                 公共js目录
        |   |   |-- common.js       公共js
        |   |-- style\              公共样式文件
        |       |-- common.scss     公共scss
        |-- pages\                  页面目录
        |   |-- about\              about页面目录
        |   |   |-- about.html      about页面结构
        |   |   |-- about.js        about页面入口
        |   |-- index\              index页面目录
        |       |-- index.html      index页面结构
        |       |-- index.js        index页面入口
        |-- utils\                  工具文件夹

```